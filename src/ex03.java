import java.util.Arrays;

/**
 * Created by yzhb363 on 30/05/2017.
 */
public class ex03 {

    public static void main(String[] args) {
        long startTime;
        long endTime;

        int max_password_length = 3;
        String[] charset = "abcdefghijklmnopqrstuvwxyz0123456789".split("");
        int[] indicies = new int[max_password_length];

        Arrays.fill(indicies, -1);
        startTime=System.currentTimeMillis();
        while (true) {
            for (int idx = indicies.length - 1; idx >= 0; idx--) {
                indicies[idx]++;

                if (indicies[idx] == charset.length) {
                    indicies[idx] = 0;
                    continue;
                }

                break;
            }

            String password = "";

            for (int i : indicies) {
                if (i != -1) {
                    password += charset[i];
                }
            }


            byte[] salt = new byte[1];

            for (int j = 0; j <= 256; j++) {


                salt[0] = (byte) j;//start with 0


                // Do password checking stuff here, break when you are done
                byte[] expectecHash = Passwords.base64Decode("vIhaK/N/w9ny3vwnE766DI2BbdmnRK8Oc5gx7zOsszQP+NOBbX0avHpJj+nWdv22J8iJtZQKBdbWFd+S30aYMw=="); //expectedHash  the expected hashed value of the password

                char[] newPassWord = password.toCharArray();

                Boolean result = Passwords.isExpectedPassword(newPassWord, salt, 5000, expectecHash);

                if (result) {
                    endTime = System.currentTimeMillis(); //password is a6
                    System.out.println("password is:" + password + "\n" + "It tooks " + (endTime - startTime));
                    break;
                }

            }
        }
    }


}
