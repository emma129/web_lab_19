import java.util.Arrays;

/**
 * Created by yzhb363 on 30/05/2017.
 */
public class ex02 {
    public static void main(String[] args) {
        long startTime;
        long endTime;
        int max_password_length = 4;
        String[] charset = "abcdefghijklmnopqrstuvwxyz0123456789".split("");
        int[] indicies = new int[max_password_length];

        Arrays.fill(indicies, -1);
        startTime = System.currentTimeMillis();
        while (true) {
            for (int idx = indicies.length - 1; idx >= 0; idx--) {
                indicies[idx]++;

                if (indicies[idx] == charset.length) {
                    indicies[idx] = 0;
                    continue;
                }

                break;
            }

            String password = "";

            for (int i : indicies) {
                if (i != -1) {
                    password += charset[i];
                }
            }

            // Do password checking stuff here, break when you are done


            byte[] salt = new byte[1];

            for (int j = 0; j <= 256; j++) {


                salt[0] = (byte) j;//start with 0


                byte[] expectecHash = Passwords.base64Decode("ocxgyfryiU2BTwWn50EO6GF5me7gDjvoKvTFiRExsEkxc7qjr8dQxT/nyMfKBv+DSvajSXPsfFCR9zVg4v9Ldg==");

                char[] newPassWord = password.toCharArray();


                if (Passwords.isExpectedPassword(newPassWord, salt, 5, expectecHash)) {

                    endTime = System.currentTimeMillis(); //password is bob
                    System.out.println("password is:" + password + "\n" + "It tooks " + (endTime - startTime));
                    break;


                }
            }



        }
    }


}
