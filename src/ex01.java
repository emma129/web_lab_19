/**
 * Created by yzhb363 on 23/05/2017.
 */
import java.util.Arrays;

public class ex01 {


    public static void main(String[] args) {
        long startTime;
        long endTime;

        int max_password_length = 5;
        String[] charset = "abcdefghijklmnopqrstuvwxyz0123456789".split("");
        int[] indicies = new int[max_password_length];

        Arrays.fill(indicies, -1);
        startTime=System.currentTimeMillis();
        while (true) {
            for (int idx = indicies.length - 1; idx >= 0; idx--) {
                indicies[idx]++;

                if (indicies[idx] == charset.length) {
                    indicies[idx] = 0;
                    continue;
                }

                break;
            }

            String password = "";

            for (int i : indicies) {
                if (i != -1) {
                    password += charset[i];
                }
            }

            // Do password checking stuff here, break when you are done
            byte[] pass=Passwords.base64Decode("wJmmp8BE3aqU8HpOQ8vVmtE9P0r6ZXh3uiHP+ZQx3CU9e6pcjD14ay7j+ljBs+gGicEcKS+pKdHn6VUpDGFgnQ=="); //expectedHash  the expected hashed value of the password
            Boolean result=Passwords.isInsecureHashMatch(password,pass);
            if(result){
                endTime = System.currentTimeMillis();
                System.out.println("password is:"+password +"\n" + "It tooks " + (endTime-startTime));
                break;
            }


        }
    }
}